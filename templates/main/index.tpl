<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title> <? echo $PageTitle; ?></title>
    <meta name="keywords" content="<? echo $PageKeywords; ?>" />
    <meta name="description" content="<? echo $PageDescription; ?>" />
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <ul id="navigation-menu">
        <li><a href="/index.php">Главная</a></li>
        <li><a href="/Users">Регистрация</a></li>
        <li><a href="/News">Новости</a></li>
    </ul>
    <h1><? echo $Title ?></h1>
    <div><? echo $Content ?></div>
</body>
</html>