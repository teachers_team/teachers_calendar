<?php
class Main_Controller
{
    protected static $template;

    public static function Init()
    {
        self::$template = new Template('templates/main/index.tpl');
    }

    public static function Route()
    {
        $parts = explode('/', $_GET['url']);
        $class = ucfirst($parts[0]) . '_Controller';
        $method = ucfirst($parts[1]) . 'Action';
        $params = array();
        for ($i = 2; $i < sizeof($parts); $i += 2) {
            $params[$parts[$i]] = $parts[$i+1];
        }
        if ($parts[0] === '' || $parts[0] === 'index.php')
            $class = 'Main_Controller';
        if (class_exists($class)) {
            if ($method === 'Action')
                $method = 'IndexAction';
            if (method_exists($class, $method)) {
                $class::$method(self::$template, $params);
            } else {
                self::Error(404);
            }
        } else {
            self::Error(404);
        }
    }

    public static function IndexAction()
    {
        $Title = $PageTitle = 'Главная страница';
        $Content = "Hello!";
        self::$template->AddParam('Title', $Title);
        self::$template->AddParam('Content', $Content);
    }

    public static function Error($code)
    {
        echo "Error $code";
        if ($code == 404)
            self::$template->AddParam('Title', 'Страница не найдена');
    }

    public static function Render()
    {
        self::$template->Display();
    }
}