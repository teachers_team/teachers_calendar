<?php
    class News_Controller
    {
        public static function IndexAction($template, $params=array())
        {
            $Title = 'Новости';
            $Content = Template::GetContents('templates/news/news.tpl');
            $template->AddParam('Title', $Title);
            $template->AddParam('Content', $Content);
        }
    }