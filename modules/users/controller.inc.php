<?php
class Users_Controller
{
    public static function RegisterAction($template, $params=array())
    {
        $Title = 'Данные регистрации';
        $Content = Template::GetContents('templates/users/display.tpl');
        $template->AddParam('Title', $Title);
        $template->AddParam('Content', $Content);
    }

    public static function IndexAction($template, $params=array())
    {
        $Title = 'Регистрация';
        $Content = Template::GetContents('templates/users/register.tpl');
        $template->AddParam('Title', $Title);
        $template->AddParam('Content', $Content);
    }
}