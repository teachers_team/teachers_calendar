<?php
    function __autoload($class)
    {
        $path = "classes/{$class}.inc.php";
        if (is_file($path))
            include_once($path);
        $arr = explode('_', strtolower($class));
        $path = "modules/{$arr[0]}/{$arr[1]}.inc.php";
        if (is_file($path))
            include_once($path);
    }

    Main_Controller::Init();
    Main_Controller::Route();
    Main_Controller::Render();